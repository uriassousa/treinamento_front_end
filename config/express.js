const express = require('express');
const bodyParser = require('body-parser');
const load = require('express-load');

module.exports = function(){

	app = express();

	app.set('view engine', 'ejs');
	app.set('views', './app/views');

	app.use(bodyParser.urlencoded({extended: true}));
	app.use(bodyParser.json());

	load('rotas', { cwd: 'app'}).into(app);

}





